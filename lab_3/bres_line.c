#include<graphics.h>
#include<math.h>
#include<stdio.h>


int main(int argc, char *argv[])
{
  int x1,x2;

  float y1,y2, m;
// /*
  x1 = atoi(argv[1]);
  y1 = atof(argv[2]);
  x2 = atoi(argv[3]);
  y2 = atof(argv[4]);
// */
  /*
  printf("Enter the x y cordinates of first point\n");
  scanf("%d",&x1);
  scanf("%f",&y1);
  //wait_for_char();
  getch();
  printf("Enter the x y cordinates of second point\n",x1,y1);
  scanf("%d",&x2);
  scanf("%f",&y2);
// */
  // have to shift the origin otherwise the line will be in the text we typed
  
 /* //wait_for_char();
  if(x2 !=x1)
  m = (float)(y2-y1)/(float)(x2-x1);
  else
  m = 1.0;
  */

  int x_inc,y_inc;
  int dx = x2-x1;
  int dy = y2-y1;

  if(dy < 0)
  {
    dy = -dy;
    y_inc = -1; // we go the other way
  }
  else
    y_inc = 1;

  if(dx < 0)
  {
    dx = -dx;
    x_inc = -1;
  }

  else if (dx ==0)
    x_inc = 0;
  else
    x_inc = 1;

  dy <<=1;
  dx <<=1;

  int gd = DETECT, gm;
  initgraph(&gd, &gm, NULL); // very important <F5>to call for initiating graphics
  
  int error = 0; // the error here is being reduced to integer so hence the given tests in the loop

  if( (x1>=0) && (y1 >=0))
    putpixel(x1, y1, RED);

  // for the case when slope is less than 1
  if(dx > dy) {
      error = dy - (dx/2);
    while(x1!=x2 )
  {
    x1 += x_inc;
    if(error >=0)
    {
      y1+= y_inc;
      error -= dx;
    }
    error+= dy;

    putpixel(x1, y1, RED );
  }

  }

  else {
    error = dx - (dy/2);
    while(y1!=y2 )
  {
    if(error >=0)
    {
      x1+= x_inc;
      error -= dx;
    }
    y1+=y_inc;
    error+= dx;
    putpixel(x1, y1, RED );

  }

  }



  getch();
  closegraph(); // has to be called when done
  

  return 0;
}



