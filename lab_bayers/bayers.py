import pygame as pg
import numpy as np
from copy import copy, deepcopy
import random

ROW =200
COL =200
tile_size =5
width, height = COL*tile_size, ROW*tile_size

RED = (175, 0, 0)
GREEN = (0, 255, 0)
BLUE = (0, 0, 255)
WHITE = (255, 255, 255)

dir = [
(0,0),
(1,0),
(0,1),
(0,-1),
(-1,0),
(1,1),
(1,-1),
(-1,1),
(-1,-1)
]

def Disp_Grid(pg,background,screen,color_grid):

  global ROW
  global COL
  global tile_size
  global GREEN
  global BLUE
  global RED
  global BLUE
  
  # global screen


  for y,i in zip(range(0, height, tile_size),range(0,ROW)):
    for x,j in zip(range(0, width, tile_size),range(0,COL)):
      rect = (x, y, tile_size, tile_size)
      color = color_grid[i][j]
      # if(color[0]==1):
      pg.draw.rect(background, (color[0+1],color[0+2],color[0+3]), rect)
      # elif(color[0]==2):
        # pg.draw.rect(background, (0,color[0+2],0), rect)

      #Source
      # elif(color[0]==3):
        # pg.draw.rect(background,(0,0,color[0+3]),rect)

      #Destination
      # elif(color_grid[i][j]==3):
      #   pg.draw.rect(background,pg.Color(0,100,0),rect)

      # elif(color_grid[i][j]==4):
      #   pg.draw.rect(background,accept_tile,rect)

      # else:
        # pg.draw.rect(background,pg.Color(183,255,183),rect)
  #pg.draw.rect(background,button_cont,(600,250,100,50))
  #screen.fill((50, 70, 90))
  screen.blit(background, (10, 10))

  #button("GO!",700,350,100,100,button_cont,button_cont_hover)
  


  pg.display.update()
  # clock.tick(30)


def main():

  global width
  global height
  global WHITE
  global RED
  global GREEN
  global BLUE
  global ROW
  size = [900, 900]

  screen = pg.display.set_mode(size)
  pg.display.set_caption("bayers")
  clock =  pg.time.Clock()

  grid = []
  for i in range(int(ROW)):
    dummy = []

    for j in range(int(COL)):

      set_red = (1,random.randint(0,255),0,0)
      set_green = (2,0,random.randint(0,255),0)
      set_blue = (3,0,0,random.randint(0,255))
      if i%2==0:
        dummy.append(set_red)
        dummy.append(set_green)
      else:
        dummy.append(set_green)
        dummy.append(set_blue)
    grid.append(dummy)
    # grid.append(np.tile([[2,random.randint(0, 255)],[3,random.randint(0, 255)]],COL))
    # grid.append(np.tile([1,2],10))
    # grid.append(np.tile([2,3],10))
    # grid.append(np.tile([1,2],10))
    # grid.append(np.tile([2,3],10))
    # grid.append(np.tile([1,2],10))
    # grid.append(np.tile([(2,11),(3, 15)],5))
    # grid.append(np.tile([1,2],10))
  
  temp = grid
  print(grid[0])
  print(temp[1])
  color_grid = deepcopy(grid)
  mosaic_grid = deepcopy(grid)

  mosaic_grid= mosaicing(color_grid, mosaic_grid)
  print(mosaic_grid[8])
  print(grid[8])

  background = pg.Surface((width, height))
  game_exit = False
  while not game_exit:
    screen.fill((WHITE))
    Disp_Grid(pg, background, screen, color_grid)
    events = pg.event.get()
    for event in events:
      if event.type == pg.QUIT:
        game_exit = True

    pg.time.wait(1000)
    screen.fill((WHITE))
    clock.tick(1)
    Disp_Grid(pg, background, screen, mosaic_grid)
    pg.time.wait(1000)





def isValid(row,col):
    global ROW
    global COL
    if((row>=0) and (row<ROW) and(col>=0) and (col<COL)==True):
      
      return True
    else:
      return False

def mosaicing(color_grid, mosaic_grid):

  global ROW
  global COL
  global tile_size
  global GREEN
  global BLUE
  global RED
  global BLUE

  for y,i in zip(range(0, height, tile_size),range(0,ROW)):
    for x,j in zip(range(0, width, tile_size),range(0,COL)):
      green_counter =0
      green_sum = 0
      blue_counter =0
      blue_sum = 0
      red_counter =0
      red_sum = 0
      for k in range(9):
        a,b = dir[k]
        if(isValid(i+a,j+b)==True):
          color = color_grid[i+a][j+b]
          if(color[0]==1):
            # print(i,j,"red")
            red_sum+=color[0+1]
            red_counter+=1
          elif(color[0]==2):
            green_sum+=color[0+2]
            green_counter+=1
          elif(color[0]==3):
            blue_sum+=color[0+3]
            blue_counter+=1
      # print(red_counter,red_sum,green_counter,green_sum,blue_counter,blue_sum)


      if blue_counter==0:
        blue_sum = blue_sum
      else:
        blue_sum = blue_sum / blue_counter

      if green_counter==0:
        green_sum = green_sum
      else:
        green_sum = green_sum / green_counter

      if red_counter==0:
        red_sum = red_sum
      else:
        red_sum = red_sum / red_counter
      mosaic_grid[i][j] = (0,
                           int(red_sum),
                           int(green_sum),
                           int(blue_sum))

  return mosaic_grid


if __name__ == '__main__':
  main()
