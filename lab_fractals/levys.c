
#include<graphics.h>
#include<math.h>


typedef struct point{

  float x,y;
}point;

void levy(int depth, point, point);

int main(int argc, char *argv[])
{
  int depth;
  point p1, p2;
  depth = atoi(argv[1]); 
  p1.x = atof(argv[2]);
  p1.y = atof(argv[3]);
  p2.x = atof(argv[4]);
  p2.y = atof(argv[5]);

  int gd =DETECT, gm;

  initgraph(&gd, &gm, NULL);

  setcolor(RED);
  levy(depth, p1, p2);

  getch();

  closegraph();
  return 0;
}


/*
 *
 *   ._________________________.
 *   p1                        p2
 *
 */

void levy( int depth, point p1, point p2)
{
 point next;

 if(!depth)
 {
  
  line(round(p1.x), round(p1.y), round(p2.x), round(p2.y));
  return;
 }

 else
 {
  next.x = p1.x + ((p2.y -p1.y) + (p2.x-p1.x))/2;
  // the sign ------------------. is negetive since y grows down in grphics :P
  next.y = p1.y + ((p2.y -p1.y) - (p2.x-p1.x))/2;

  // order of points is imp ie p1,next is different from next,p1
  levy(depth-1, p1,next);
  levy(depth-1, next, p2);
 }

}
