
// This variable will store the WebGL rendering context
var gl;

window.onload = function init() {
  // Set up a WebGL Rendering Context in an HTML5 Canvas
  var canvas = document.getElementById("gl-canvas");
  // gl = WebGLUtils.setupWebGL(canvas);
  gl = canvas.getContext("webgl");
  if (!gl) {
    alert("WebGL isn't available");
  }

  //  Configure WebGL
  //  eg. - set a clear color
  //      - turn on depth testing
  resize(gl.canvas)

  // gl.clearColor(0.9, 0.9, 0.9, 1.0);

  //  Load shaders and initialize attribute buffers
  var program = initShaders(gl, "vertex-shader", "fragment-shader");
  // var program = webglUtils.createProgramFromScripts(gl,["vertex-shader", "fragment-shader"])
  gl.useProgram(program);


  // Set up data to draw
  // var vertices = [
  //           0.5, 0.5, // point 1
  //            -0.5, -0.5, // point 2
  //            0.5, 0.5,// point 2
  //            -0.5, 0.5
          
  //       ];
  

  
//   var colors= 
// [
//    vec4( 1.0, 0.0, 0.0, 1.0 ), // Triangle 1 is red
//    vec4( 1.0, 0.0, 0.0, 1.0 ), 
//    vec4( 1.0, 0.0, 0.0, 1.0 ),
//    vec4( 0.0, 1.0, 1.0, 1.0 ), // Triangle 2 is cyan
//    vec4( 0.0, 1.0, 1.0, 1.0 ),
//    vec4( 0.0, 1.0, 1.0, 1.0 )
// ];


// var positions = [
//     -1.0, -1.0,  1.0,
//      1.0, -1.0,  1.0,
//      1.0,  1.0,  1.0,
//     -1.0,  1.0,  1.0,
//   ];

  var positions = [
    100, 200,
    200, 200,
    100, 300,
    100, 300,
    200, 200,
    200, 300
  ];

var size = 3;          // 2 components per iteration
  var type = gl.FLOAT;   // the data is 32bit floats
  var normalize = false; // don't normalize the data
  var stride = 0;        // 0 = move forward size * sizeof(type) each iteration to get the next position
  var offset = 0;        // start at the beginning of the buffer




  // Load the data into GPU data buffers
// The vertex array is copied into one buffer
  // var vertex_buffer = gl.createBuffer();
  // gl.bindBuffer(gl.ARRAY_BUFFER, vertex_buffer);
  // gl.bufferData(gl.ARRAY_BUFFER, flatten(vertices), gl.STATIC_DRAW);
  // gl.bindBuffer(gl.ARRAY_BUFFER, null);
  // document.write(flatten(vertices))
  // The colour array is copied into another
  // var color_buffer = gl.createBuffer();
  // gl.bindBuffer(gl.ARRAY_BUFFER, color_buffer);
  // gl.bufferData(gl.ARRAY_BUFFER, flatten(colors), gl.STATIC_DRAW);
  
  // for position
    var position_buffer = gl.createBuffer();
  gl.bindBuffer(gl.ARRAY_BUFFER, position_buffer);
  gl.bufferData(gl.ARRAY_BUFFER, flatten(positions), gl.STATIC_DRAW);
  
  
  
  








  gl.bindBuffer(gl.ARRAY_BUFFER, position_buffer);
  var a_Position = gl.getAttribLocation(program, "a_Position");
  var u_resolution = gl.getUniformLocation(program, "u_resolution");
  gl.vertexAttribPointer(a_Position, 2, gl.FLOAT, false, 0, 0);
  gl.enableVertexAttribArray(a_Position);


  const indexBuffer = gl.createBuffer();
  gl.bindBuffer(gl.ELEMENT_ARRAY_BUFFER, indexBuffer);

  // This array defines each face as two triangles, using the
  // indices into the vertex array to specify each triangle's
  // position.

//   const indices = [
//     0,  1,  2,      0,  2,  3    // front
    
//   ];

//   // Now send the element array to GL

//   gl.bufferData(gl.ELEMENT_ARRAY_BUFFER,
//       new Uint16Array(indices), gl.STATIC_DRAW);
// gl.bindBuffer(gl.ELEMENT_ARRAY_BUFFER, indexBuffer);







  { 

  var texture = loadTexture(gl, "pic.jpeg");
    var texture_buffer = gl.createBuffer();
  var tex_Position = gl.getUniformLocation(program, "a_texcoord");
    var u_texture = gl.getUniformLocation(program, "u_texture");
  

  // var tex_positions = positions.map(function(element){
  //   // element = element /255;
  //   // element = element *2.0;
  //   // element = element -1.0;
  //   return element * 1;
  // })
  var tex_positions = [
  
    100, 200,
    200, 200,
    100, 300,
    100, 300,
    200, 200,
    200, 300

  ]

  // var tex_positions = [
  
  //   0.0,  0.0,
  //   1.0,  0.0,
  //   1.0,  1.0,
  //   0.0,  1.0

  // ]

  gl.bindBuffer(gl.ARRAY_BUFFER, texture_buffer);

  gl.bufferData(gl.ARRAY_BUFFER, flatten(tex_positions), gl.STATIC_DRAW);
  gl.vertexAttribPointer(tex_Position, size-1, type, normalize, stride, offset);
  gl.enableVertexAttribArray(tex_Position);

    gl.activeTexture(gl.TEXTURE0);
    gl.bindTexture(gl.TEXTURE_2D, texture);
    gl.uniform1i(u_texture, 0);


}


















// set the resolution
  gl.uniform2f(u_resolution, gl.canvas.width, gl.canvas.height);



  // Get addresses of shader uniforms

  // Either draw as part of initialization
  render();

  // Or draw just before the next repaint event
  //requestAnimFrame(render());
};


function render() {
   // clear the screen
// Actually, the  WebGL automatically clears the screen before drawing.
  // This command will clear the screen to the clear color instead of white.

  gl.clear(gl.COLOR_BUFFER_BIT);
  gl.clearColor(0.5, 0.5, 0.5, 0.9);
  gl.enable(gl.DEPTH_TEST);
   gl.clear(gl.COLOR_BUFFER_BIT | gl.DEPTH_BUFFER_BIT);
  // gl.clearColor(0, 0, 0, 0);
  // gl.clear(gl.COLOR_BUFFER_BIT);
    gl.viewport(0, 0, gl.canvas.width, gl.canvas.height);
 
  // draw
  // Draw the data from the buffers currently associated with shader variables
  // Our triangle has three vertices that start at the beginning of the buffer.
  gl.drawArrays(gl.TRIANGLE_STRIP, 0, 6);

  // const vertexCount = 6;
  // const type = gl.UNSIGNED_SHORT;
  // const offset = 0;
  // gl.drawElements(gl.TRIANGLES, vertexCount, type, offset);


}


