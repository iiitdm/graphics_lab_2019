
#include<graphics.h>
#include<math.h>


typedef struct point{

  float x,y;
}point;

void draw_triangle(point p1, point p2, point p3);

void draw_line_curve(int depth, point p1, point p2, int sign, int);
void snow_flake(int depth, point p1, point p2, point p3);

point div_ratio(point p1, point p2, int m, int n);

int main(int argc, char *argv[])
{
  point p1, p2, p3;
  int depth = atoi(argv[1]); 
  p1.x = atof(argv[2]);
  p1.y = atof(argv[3]);
  p2.x = atof(argv[4]);
  p2.y = atof(argv[5]);
  p3.x = atof(argv[6]);
  p3.y = atof(argv[7]);

  int gd = DETECT, gm;

  initgraph(&gd, &gm, NULL);

  snow_flake(depth, p1, p2, p3);

  getch();

  closegraph();

  return 0;
}

// points of triangle in clockwise

void snow_flake(int depth, point p1, point p2, point p3)
{
  /*
  if(depth <=0)
  {
    draw_triangle(p1, p2, p3);
    return;
  }

  */

  //grows towards positive
  setcolor(RED);
  draw_line_curve(depth, p1, p2, 1, -1);
  setcolor(GREEN);
  draw_line_curve(depth, p3, p1, 1, -1);
  setcolor(BLUE);
  draw_line_curve(depth, p2, p3, 1, -1);
}


void draw_line_curve(int depth, point p1, point p2, int sign1, int sign2)
{
  point n_top, n_left, n_right;

 if(!depth)
 {
  
  line(round(p1.x), round(p1.y), round(p2.x), round(p2.y));
  return;
 }

 else
 {
    n_right = div_ratio(p1, p2, 2, 1);
    n_left = div_ratio(p1, p2, 1, 2);

    n_top.x = n_left.x + (sign1*((n_right.y -n_left.y) +(n_right.x-n_left.x)))/2;
  // the sign ------------------. is negetive since y grows down in grphics :P
    n_top.y = n_left.y + ((n_right.y -n_left.y) +(sign2* (n_right.x-n_left.x)))/2;

    
  //line(round(p1.x), round(p1.y), round(n_left.x), round(n_left.y));
  //line(round(n_right.x), round(n_right.y), round(p2.x), round(p2.y));


  // order of points is imp ie p1,next is different from next,p1
  draw_line_curve(depth-1, p1, n_left, sign1, sign2);
  draw_line_curve(depth-1, n_right, p2, sign1, sign2);
  draw_line_curve(depth-1, n_left, n_top, sign1, sign2);
  draw_line_curve(depth-1, n_top, n_right, sign1, sign2);

    
 }
  
}


point div_ratio(point p1, point p2, int m, int n)
{

  point p3;
  p3.x = ((p2.x * (float)m) + (p1.x * (float)n)) /(float)(m+n);
  p3.y = ((p2.y * (float)m) + (p1.y * (float)n)) /(float)(m+n);

    return p3;
}

/*
void draw_triangle(point p1, point p2, point p3)
{
  setcolor(RED);
  line(round(p1.x), round(p1.y), round(p2.x), round(p2.y));
  setcolor(GREEN);
  line(round(p1.x), round(p1.y), round(p3.x), round(p3.y));
  setcolor(BLACK);
  line(round(p3.x), round(p3.y), round(p2.x), round(p2.y));


}*/

