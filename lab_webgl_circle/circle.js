

// This variable will store the WebGL rendering context
var gl;

window.onload = function init() {
  

  var r = 50;


  // Set up a WebGL Rendering Context in an HTML5 Canvas
  var canvas = document.getElementById("gl-canvas");
  // gl = WebGLUtils.setupWebGL(canvas);
  gl = canvas.getContext("webgl");
  if (!gl) {
    alert("WebGL isn't available");
  }

  //  Configure WebGL
  //  eg. - set a clear color
  //      - turn on depth testing
  resize(gl.canvas)

  // gl.clearColor(0.9, 0.9, 0.9, 1.0);

  //  Load shaders and initialize attribute buffers
  var program = initShaders(gl, "vertex-shader", "fragment-shader");
  // var program = webglUtils.createProgramFromScripts(gl,["vertex-shader", "fragment-shader"])
  gl.useProgram(program);

  

  // var positions = [
  //   100, 200,
  //   200, 200,
  //   100, 300,
  //   100, 300,
  //   200, 200,
  //   200, 300
  // ];


    var colors= 
[
   vec4( 1.0, 0.0, 0.0, 1.0 ), // Triangle 1 is red
   vec4( 1.0, 0.0, 0.0, 1.0 ), 
   vec4( 1.0, 0.0, 0.0, 1.0 ),
   vec4( 0.0, 1.0, 1.0, 1.0 ), // Triangle 2 is cyan
   vec4( 0.0, 1.0, 1.0, 1.0 ),
   vec4( 0.0, 1.0, 1.0, 1.0 )
];


  var positions = []


  // circle creation
  
  // center
  var x = 100;
  var y = 100;

  var x1 = 0;
  var y1 = r;

  var error = 3 - (2*r);
  while(x1<y1)
  {

    positions.push(x+x1);
    positions.push(y+y1);
    positions.push(x+x1);
    positions.push(y-y1);

    positions.push(x-x1);
    positions.push(y+y1);
    positions.push(x-x1);
    positions.push(y-y1);

    positions.push(x+y1);
    positions.push(y+x1);
    positions.push(x-y1);
    positions.push(y+x1);

    positions.push(x+y1);
    positions.push(y-x1);
    positions.push(x-y1);
    positions.push(y-x1);

    if(error<0)
    {
      error += (4*x1)+6;
    }

    else
    {
        y1--;
        error+=(4*x1) + 10 -(4*y1);
    }

    x1++;
  }


var size = 3;          // 2 components per iteration
  var type = gl.FLOAT;   // the data is 32bit floats
  var normalize = false; // don't normalize the data
  var stride = 0;        // 0 = move forward size * sizeof(type) each iteration to get the next position
  var offset = 0;        // start at the beginning of the buffer




  // Load the data into GPU data buffers
// The vertex array is copied into one buffer
  // var vertex_buffer = gl.createBuffer();
  // gl.bindBuffer(gl.ARRAY_BUFFER, vertex_buffer);
  // gl.bufferData(gl.ARRAY_BUFFER, flatten(vertices), gl.STATIC_DRAW);
  // gl.bindBuffer(gl.ARRAY_BUFFER, null);
  // document.write(flatten(vertices))
  // The colour array is copied into another
  var color_buffer = gl.createBuffer();
  gl.bindBuffer(gl.ARRAY_BUFFER, color_buffer);
  gl.bufferData(gl.ARRAY_BUFFER, flatten(colors), gl.STATIC_DRAW);
  
  // for position
    var position_buffer = gl.createBuffer();
  gl.bindBuffer(gl.ARRAY_BUFFER, position_buffer);
  gl.bufferData(gl.ARRAY_BUFFER, flatten(positions), gl.STATIC_DRAW);
  
  
  
  
  //Here we prepare the "vColor" shader attribute entry point to
  //receive RGB float colours from the colour buffer
  
  // var vColor = gl.getAttribLocation(program, "vColor");
  // gl.bindBuffer(gl.ARRAY_BUFFER, color_buffer);
  // gl.vertexAttribPointer(vColor, 0, gl.FLOAT, false, 0, 0);
  // gl.enableVertexAttribArray(vColor);
  


// Associate shader attributes with corresponding data buffers
//Here we prepare the "a_Position" shader attribute entry point to
  //receive 2D float vertex positi:q
  //ons from the vertex buffer
  gl.bindBuffer(gl.ARRAY_BUFFER, position_buffer);
  var a_Position = gl.getAttribLocation(program, "a_Position");
  var u_resolution = gl.getUniformLocation(program, "u_resolution");
  gl.vertexAttribPointer(a_Position, 2, gl.FLOAT, false, 0, 0);
  gl.enableVertexAttribArray(a_Position);


  


// set the resolution
  gl.uniform2f(u_resolution, gl.canvas.width, gl.canvas.height);



  // Get addresses of shader uniforms

  // Either draw as part of initialization
  console.log(positions.length);
  render(positions.length);



  // Or draw just before the next repaint event
  //requestAnimFrame(render());
};


function render(length) {
   // clear the screen
// Actually, the  WebGL automatically clears the screen before drawing.
  // This command will clear the screen to the clear color instead of white.

  gl.clear(gl.COLOR_BUFFER_BIT);
  gl.clearColor(0.5, 0.5, 0.5, 0.9);
  gl.enable(gl.DEPTH_TEST);
   gl.clear(gl.COLOR_BUFFER_BIT | gl.DEPTH_BUFFER_BIT);
  // gl.clearColor(0, 0, 0, 0);
  // gl.clear(gl.COLOR_BUFFER_BIT);
    gl.viewport(0, 0, gl.canvas.width, gl.canvas.height);
 
  // draw
  // Draw the data from the buffers currently associated with shader variables
  // Our triangle has three vertices that start at the beginning of the buffer.
  console.log(length);
  gl.drawArrays(gl.TRIANGLES, 0, length/2);



}
