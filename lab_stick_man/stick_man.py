import pygame as pg
import random
from operator import add

pg.init()

BLACK = (0, 0, 0)
WHITE = (255, 255, 255)
RED = (255, 0, 0)

size = [500, 500]

def main():
  screen = pg.display.set_mode(size)

  pg.display.set_caption("Chiru Style")


  snow_list=[]

  for i in range(50):
    x = random.randrange(0, 400)
    y = random.randrange(0, 400)
    snow_list.append([x, y])


  clock = pg.time.Clock()

  done = False

  x,y = 1,1
  i = 0

  sign = 1

  while not done:

    for event in pg.event.get():
      if event.type == pg.event.get():
        if even.type == pg.event.QUIT:
          done = True

    screen.fill(BLACK)

    abdoman = draw_upper_body(pg, screen, x, y)
    knee_x = draw_lower_body(pg, screen, abdoman,x, y, i)
    pg.display.flip()

    if i == 0:
      i=1
    else:
      i=0

    clock.tick(5)
    # print(knee_x)
    
    x = x+ (1*(sign))
    y = y+ (-1*(sign))

    if knee_x +7 == 250:
      sign = -1
    elif knee_x +1==226 :
      sign = 1

  pg.quit()




def draw_lower_body(pg, screen, abdoman, x, y, i):

  up_dn = [0,3]
  knee_left = list(map(add, abdoman, [-25+x,2]))
  knee_right = list(map(add, abdoman, [25-x,2]))

  knee_left[1] = 296
  knee_right[1] = 296
  pg.draw.line(screen, WHITE, knee_right, abdoman, 1)
  pg.draw.line(screen, WHITE, knee_left, abdoman, 1)

  ankle_left = [[243, 313], [243, 315]]
  ankle_right =[[257, 317], [257, 313]]
  # ankle_left = list(map(add,
  #                       knee_left,[17,17 + up_dn[i]] ))
  # ankle_right = list(map(add,
  #                        knee_right,[-17,17 + up_dn[::-1][i]] ))

  

  pg.draw.line(screen, WHITE, knee_right, ankle_right[i], 1)
  pg.draw.line(screen, WHITE, knee_left, ankle_left[i], 1)

  # toe_right = list(map(add, ankle_right, [10,up_dn[i]]))
  # toe_left = list(map(add, ankle_left, [-10,up_dn[::-1][i]]))

  toe_right = list(map(add, ankle_right[i], [7,up_dn[i]]))
  toe_left = list(map(add, ankle_left[i], [-7,up_dn[::-1][i]]))



  pg.draw.line(screen, WHITE, toe_right, ankle_right[i], 1)
  pg.draw.line(screen, WHITE, toe_left, ankle_left[i], 1)

  print(knee_left,knee_right)#,ankle_left,ankle_right)
  return knee_left[0]


def draw_upper_body(pg, screen, x, y):
  """
  function to draw the constant upper body
  with some variation in height
  """
  center = [250, 250+y]
  # print(type(center))
  eye_left = list(map(add, center, [ -4+x, -2]))
  eye_right = list(map(add, center, [ 4+x, -2]))

  smile_left = list(map(add, center, [ -3+x, 3]))
  smile_right = list(map(add, center, [ 3+x, 3]))


  pg.draw.circle(screen, WHITE, center, 13)
  pg.draw.circle(screen, BLACK, eye_right, 1)
  pg.draw.circle(screen, BLACK, eye_left, 1)
  pg.draw.line(screen, BLACK, smile_right, smile_left, 1)

  neck = list(map(add, center,[0, 20]))
  # print(type(neck), neck[0], neck [1])
  pg.draw.line(screen, WHITE, center, neck, 1)
  
  elbow_left = list(map(add, neck, [-30, 5]))
  elbow_right = list(map(add, neck, [30, 2]))
  pg.draw.line(screen, WHITE, neck, elbow_right, 1)
  pg.draw.line(screen, WHITE, neck, elbow_left, 1)

  hand_left = list(map(add, elbow_left, [4,17]))
  hand_right = list(map(add, elbow_right, [0,-17]))
  pg.draw.line(screen, WHITE, hand_right, elbow_right, 1)
  pg.draw.line(screen, WHITE, hand_left, elbow_left, 1)

  pg.draw.line(screen, RED, hand_left, hand_right, 5)
  pg.draw.line(screen, WHITE, hand_left, hand_right, 1)


  abdoman = list(map(add, neck, [0, 23]))
  pg.draw.line(screen, WHITE, neck, abdoman, 1)

  # knee_left = list(map(add, abdoman, [-25,2]))
  # knee_right = list(map(add, abdoman, [25,2+1]))
  # pg.draw.line(screen, WHITE, knee_right, abdoman, 1)
  # pg.draw.line(screen, WHITE, knee_left, abdoman, 1)


  return abdoman  


if __name__ == '__main__':
  main()