# GRAPHICS TERM PROEJCT (DeadLine:29/10/19)

**Problem Statement:** Create a graphical animation or a game.

**Constraints:**
1. Any Language Platform can be used.
2. Project can be a continuation/modification of any project from online resources

[**This project is a modification of a project from this repo**](https://github.com/Wangxh329/WebGL-Animation)

### Descrption (The whole story)

#### The Story
The episode is about a class and 2 kids(not so much of a relavance!), they are late to their class. Once they enter the class where the teacher has already started telling the students about a story related to a chinese saying "3 people spreading reports of a tiger make you believe there is one around” -- in literal translation.

#### Tech Desc
1. **Language/Platform:** JavaScript and WebGl.
2. **Resources:**
    - [Tiny-WebGl Package](https://github.com/encyclopedia-of-code/tiny-graphics-js)
3. **Files:**
    - *assets* Director, Contains all the additional files like textures etc.
    - *index.html* , Is the driver/main file of the project.
    - *dependancies.js*, a file provided by UCLA...
    - *tinywebgl-ucla.js*, a file provided by UCLA CS 174A,  graphics course


#### My Story till now (17/10/19)

1. Understood the working of the existing project, how things are being done.
2. Created the basic background scenery.
3. Created the 2 kids(incomplete bidy) and the animation for walking.

**To Do:**
1. Have to create the tree.
2. Make the motion to the tree.
3. Understand the camera and eye animation and movements to incorporate better scene viewing.
4. Make more adjustments to the animation
5. Add music

**Screen Shots**

![Screenshot 1](screens/sc_1.png)


![Screenshot 2](screens/sc_2.png)


#### Final Story (25/10/19)

1. Created Tree. ( Procedurally generated leaves no hard coding!!)
2. Adjusted camera, eye and phong setting for better viewing.
3. Added hands and Dialogue cloud when required.
4. Procedurally generated class room.
5. Added music. [Link to the track](https://www.youtube.com/watch?v=lCOF9LN_Zxs)
6. Created a continutation to the pre built story of chinese saying.

**Screen Shots**

![Screenshot 3](screens/sc_3.png)


![Screenshot 4](screens/sc_4.png)

**Code contribution (300+, stress on quality of contribution rather than quantity!)**

```

view1             : context.get_instance( Phong_Model  ).material( Color.of( 0,0,0,1 ), .5, .5, .5, 40, context.get_instance( "assets/room/view1.jpg" ) ),
view2             : context.get_instance( Phong_Model  ).material( Color.of( 0,0,0,1 ), .5, .5, .5, 40, context.get_instance( "assets/room/view2.jpg" ) ),
view3             : context.get_instance( Phong_Model  ).material( Color.of( 0,0,0,1 ), .5, .5, .5, 40, context.get_instance( "assets/room/view3.jpg" ) ),
ground            : context.get_instance( Phong_Model  ).material( Color.of( 0,0,0,1 ), .5, .5, .5, 40, context.get_instance( "assets/room/ground.jpeg" ) ),
shirt1            : context.get_instance( Phong_Model  ).material( Color.of( 0,0,0,1 ), .5, .5, .5, 40, context.get_instance( "assets/human/shirt.jpg" ) ),
shirt2            : context.get_instance( Phong_Model  ).material( Color.of( 0,0,0,1 ), .5, .5, .5, 40, context.get_instance( "assets/human/pant.jpg" ) ),
bark             : context.get_instance( Phong_Model  ).material( Color.of( 0,0,0,1 ), .5, .5, .5, 40, context.get_instance( "assets/wood/bark.png" ) ),

// TREE
draw_leaf(graphics_state, model_transform, x, y, z, scale)
      {
          var  leaf_back_transform = model_transform.times(Mat4.scale(scale));
          // this.shapes.leaf_back.draw(graphics_state, leaf_back_transform, this.red);
  
          var leaf_front_transform = leaf_back_transform.times(Mat4.translation([z, y, x]));
          this.shapes.leaf_front.draw(graphics_state, leaf_front_transform, this.green);
  
      }

draw_tree(graphics_state, model_transform)
        { 
            var scale = [1,1,1];
              //   scale.push(Math.random(), Math.random(), Math.random());
          // y^2 + 2y = -4x+8;
          
          // leafs
          var y = 0;
          var x = 2;
          var z = -10;
          var levels = 12;
          var width = 12;
          var leaf_transform = model_transform;
          var temp;
          while(levels)
          {
              
              for(var z = -width/2; z <=width/2 ;)
         
              {   
                  
                  y = 0;
                  
                  x = 2;
                  while(y<x)
                  {
                      x = ( Math.pow(y,2) + (2*y)  - 20) / -4;
                      // if(x==0)
                      // continue;
                      y = y+ 0.2;//Math.sin(5*Math.PI/(x*4)); 
                      
                      this.draw_leaf(graphics_state, leaf_transform, x, y, z, scale);
                  }
          
                  z = z +.5;
              }
  
              leaf_transform = leaf_transform.times(Mat4.translation([0, y/3,0]));
              width = width -.8;
              levels -= 1;
          }
  
          // Bark
          var bark_transform = model_transform.times(Mat4.translation([0, -3,3])).times(Mat4.scale([1.5,5,1.5])).times(Mat4.rotation(Math.PI/2, Vec.of(1,0,0)));
          this.shapes.cylinder.draw(graphics_state, bark_transform, this.bark);
      }

 // HUMAN STUFF
    
    draw_mouth(graphics_state, model_transform, r)
      {  
        var upmouse_transform = model_transform.times(Mat4.translation([0, -.04, 1.3])).times(Mat4.scale([.5, .5, .6]));
        this.shapes.half_tra_cyl_upmouse.draw(graphics_state, upmouse_transform, this.buffalo_skin_foot);
        
        var rotate_transform = model_transform.times(Mat4.translation([0,-.4,.9])).times(Mat4.rotation(r,Vec.of( 1,0,0 )));
        var bottommouse_transform = rotate_transform.times(Mat4.translation([0, .37, .4])).times(Mat4.scale([.5, .5, .6])).times(Mat4.rotation(Math.PI, Vec.of(0, 0, 1)));
        this.shapes.half_tra_cyl_bottommouse.draw(graphics_state, bottommouse_transform, this.buffalo_skin_foot);
    }
    
    //[z,x,y] 
    //
    
    draw_human_head(graphics_state, model_transform, scale_vec1, body_texture )
    {
        // have to rotate like this in order make it look straight ( updown )
        // model_transform = model_transform.times(Mat4.rotation(-Math.PI/10, Vec.of(1,0,0)));


        var sphere_transform = model_transform.times(Mat4.scale(scale_vec1)).times(Mat4.rotation( Math.PI/2, Vec.of(1, 0, 0)));
        this.shapes.sphere.draw(graphics_state, sphere_transform, body_texture);

        var r = Math.PI / 24 + Math.PI / 24 * Math.sin(5*Math.PI / 3 * this.t);

        // eye
        for(var i of [-.50, .50])
        {
            var eye_transform = model_transform.times(Mat4.translation([i, -.6, .6])).times(Mat4.scale([.1, .1, .1]));
            this.shapes.sphere.draw(graphics_state, eye_transform, this.black);
        }
    
         //mouth
        var mouth_transform = model_transform.times(Mat4.translation([0, 1, -.1])).times(Mat4.rotation(Math.PI/2 , Vec.of(1, 0, 0))); // /2
        this.draw_mouth(graphics_state, mouth_transform, r);

        // nose
        var nose_transform = model_transform.times(Mat4.translation([0, -.6, 0.3])).times(Mat4.scale([.2, .5, .2])).times(Mat4.rotation(Math.PI/2, Vec.of(1, 0, 0)));
        this.shapes.semi_sphere.draw(graphics_state, nose_transform, this.buffalo_skin_foot);

    }

    draw_human_part(graphics_state, model_transform, body_texture) // model_transform在pill中心
    {
        this.draw_pillShape( graphics_state, model_transform, body_texture);
    }

    draw_leg_part(graphics_state, model_transform, body_texture, r)
    {
        model_transform = model_transform.times(Mat4.rotation(r, Vec.of(1,0,0)));


        this.draw_human_part(graphics_state, model_transform, body_texture);
        
        var lower_transform = model_transform.times(Mat4.translation([0, 0, -3]));

        this.draw_human_part(graphics_state, lower_transform, body_texture);

        var ring_transform = model_transform.times(Mat4.translation([0, 0, -4]));

        this.shapes.donut.draw( graphics_state, ring_transform, this.buffalo_skin_foot );

        
        var shoe_transform = model_transform.times(Mat4.scale([1.4, 1.4, 2.4])).times(Mat4.translation([0, 0, -2.5])).times(Mat4.rotation(Math.PI/8 ,Vec.of(1, 0,0)));

        shoe_transform = shoe_transform.times(Mat4.scale([1.1, 1.1, 1.1]));
        this.shapes.semi_sphere.draw(graphics_state, shoe_transform, this.buffalo_skin_foot);
    }

    draw_hand_part(graphics_state, model_transform, body_texture, r)
      {
        model_transform = model_transform.times(Mat4.rotation(r, Vec.of(1,0,0)));

        this.draw_human_part(graphics_state, model_transform, body_texture);

        var lower_transform = model_transform.times(Mat4.translation([0, 0, -4]));
  
          this.draw_human_part(graphics_state, lower_transform, body_texture);
  
      }

 draw_human(graphics_state, model_transform, scale_vec1, body_texture, cloth, move, dir)
      {
  
          if(dir == 'straight')
          {
  
              // going the other way
              model_transform = model_transform.times(Mat4.rotation(Math.PI, Vec.of(0,0,1)));      
              model_transform = model_transform.times(Mat4.rotation(Math.PI/3, Vec.of(1,0,0)));    
          }
  
          else if (dir == 'right')
          {
              // model_transform = model_transform.times(Mat4.rotation(Math.PI/6, Vec.of(0,0,1)));      
              model_transform = model_transform.times(Mat4.rotation(-Math.PI/3, Vec.of(1,0,0)));   
              model_transform = model_transform.times(Mat4.rotation(Math.PI/2, Vec.of(0,1,0))); 
              // model_transform = model_transform.times(Mat4.rotation(Math.PI/2, Vec.of(0,0,1))); 
              // model_transform = model_transform.times(Mat4.rotation(Math.PI/5, Vec.of(1,0,0))); 
          }
          
  
          var r = Math.PI / 6 * Math.sin(1.5 * Math.PI / 2 * this.t + 5*Math.PI / 6);
          
          r = r* move;
  
          // head
          var head_transform = model_transform.times(Mat4.rotation(-Math.PI/10, Vec.of(1,0,0)));
          this.draw_human_head(graphics_state, head_transform, scale_vec1,body_texture);
  
          //neck
          var neck_transform = model_transform.times(Mat4.translation([0, -.35, -.5])).times(Mat4.scale([.3, .3, .3])).times(Mat4.rotation(-Math.PI/6, Vec.of(1, 0, 0)));
          this.draw_human_part(graphics_state, neck_transform, body_texture);
  
  
          //body
          var body_transform = model_transform.times(Mat4.translation([0, -1.2, -2])).times(Mat4.scale([.6, .6, .6])).times(Mat4.rotation(-Math.PI/6, Vec.of(1, 0, 0)));
          this.draw_human_part(graphics_state, body_transform, cloth);
  
  
          //leg
  
          for(var i of [-.5, .5])
          {
              var leg_transform = model_transform.times(Mat4.translation([i, -1.6, -3])).times(Mat4.scale([.25, .25, .25])).times(Mat4.rotation(-Math.PI/6, Vec.of(1, 0,0)));
              this.draw_leg_part(graphics_state, leg_transform, body_texture, r);
              r=-r;
  
          }

          for(var i of [-.6, .6])
          {
            var leg_transform = model_transform.times(Mat4.translation([i, -1, -1.5])).times(Mat4.scale([.25, .25, .25])).times(Mat4.rotation(-Math.PI/2, Vec.of(1, 0,0)));
              this.draw_hand_part(graphics_state, leg_transform, body_texture, r);
              r=-r;
          }
          
          
        }


/// IN display function
 var my_model_transform = Mat4.identity();

 if(this.t<35)
            {
              var ground_transform = model_transform.times(Mat4.translation([0, -10, 0])).times(Mat4.scale([60, 3, 100]));
              this.shapes.box.draw(graphics_state, ground_transform, this.grass);
              var view1_transform = model_transform.times(Mat4.translation([-60, 30, 0])).times(Mat4.scale([3, 40, 100])); // front
              this.shapes.box.draw(graphics_state, view1_transform, this.view1);
              var view4_transform = model_transform.times(Mat4.translation([0, 30, -100])).times(Mat4.scale([60, 40, 3]));  // left
              this.shapes.box.draw(graphics_state, view4_transform, this.view1);
          var view2_transform = model_transform.times(Mat4.translation([0, 30, 100])).times(Mat4.scale([60, 40, 3])); //right
          this.shapes.box.draw(graphics_state, view2_transform, this.view3);
          var view3_transform = model_transform.times(Mat4.translation([50, 30, 0])).times(Mat4.scale([3, 40, 120])); // back
          this.shapes.box.draw(graphics_state, view3_transform, this.view3);
          
          var tree_transform = model_transform.times(Mat4.translation([0, 0, -75])).times(Mat4.scale([1.5,1.5,1.5]));
          this.draw_tree(graphics_state, tree_transform);
          var tree_small_transform;
          var factor ;
          
  
          
          var move = 0;
  

          
          var man_transform = model_transform;
          var teacher_transform ;
          var man2_transform = model_transform;
          var scale_vec1 = Vec.of(1, .9, 1);
          var pace_1;
          var pace_2;
          var t_move=0;
          var t_pace;
  
          var temp_at = {
              'x' : 0,
              'y' : 0,
              'z' : 0
  
          };
          var temp_eye = {
              'x' : 0,
              'y' : 0,
              'z' : 0
  
          };
          temp_eye.x =1;
          temp_eye.x =1;
          temp_eye.x =1; 
  
  
          var teacher_board_transform = model_transform.times(Mat4.translation([500, 500, 500]));
  
          
  
          var kids_transform;
          for(var j = 0; j <5; j++)
          {
  
              for(var i = 0; i<16; i++)
              {
                  if(i ==5 || i==6)
                  {
                      continue;
                  }
                  kids_transform  = model_transform.times(Mat4.translation([-25 + 2.7*i, -2, -45 + 3*j])).times(Mat4.scale([1,1,1])).times(Mat4.rotation(-Math.PI / 3,Vec.of(1,0,0)));
                  this.draw_human(graphics_state, kids_transform, scale_vec1,this.buffalo_skin_body3, this.shirt2, 0, 'straight');
              }
              
          }


          // action change
          
  
          // //if(this.t >=0)
          if(this.t >= 0)
          {   
              move = 1;
              pace_1 =.7;
              pace_2 = 1.5;
            //   pace_1 = 0;
            //   pace_2 = 0;
              t_move = 1;
              man_transform = model_transform.times(Mat4.translation([-11.5,-2, 1- pace_2*this.t])).times(Mat4.scale([1,1,1])).times(Mat4.rotation(-Math.PI / 3,Vec.of(1,0,0)));
              man2_transform = model_transform.times(Mat4.translation([-8.8,-2, 1- pace_2*this.t])).times(Mat4.scale([1,1,1])).times(Mat4.rotation(-Math.PI / 3,Vec.of(1,0,0)));
              // teacher_transform = model_transform.times(Mat4.translation([-11.5,2,-70]));
              teacher_transform = model_transform.times(Mat4.translation([-11.5,0, -60 + pace_1*this.t])).times(Mat4.scale([1.5,1.5,1.5])).times(Mat4.rotation(-Math.PI / 3,Vec.of(1,0,0)));
              
              // cam_eye = Vec.of(-20, 5, 30);
              // cam_at = Vec.of(-16, -1, -13);
  
              cam_eye = Vec.of(-25, 11, 30);
              cam_at = Vec.of(-16, -1, -3);
              // cam_eye = Vec.of(-10, 3, 20);
              // cam_at = Vec.of(-8.8, -2, 3);
          }
          if(this.t >= 2)
          {
  
              cam_eye = Vec.of(-25 - 1.2*this.t, 11, 30);
              cam_at = Vec.of(-16, -1, -3);
          }

          if(this.t >= 10)
          {
              cam_eye = Vec.of(-37, 5, 30- 1.2*this.t);
              cam_at = Vec.of(-16, -1, -15);
              
          }
  
          if(this.t >= 15)
          {   
              pace_1 = 0;
              t_move = 0;
              teacher_transform = model_transform.times(Mat4.translation([-11.5,0, -44.847 ])).times(Mat4.scale([1.5,1.5,1.5])).times(Mat4.rotation(-Math.PI / 3,Vec.of(1,0,0)));
  
              teacher_board_transform = model_transform.times(Mat4.translation([-15, 4, -44.847]));
              this.draw_talkboard(graphics_state, teacher_board_transform, texture_1);  
  
             
              cam_eye = Vec.of(-37, 5, 30- 1.5*this.t);
              cam_at = Vec.of(-16, -1, -15- 1.2*this.t);
                        
          } 
  
          if(this.t >= 21)
        {
            // console.log(1- pace_2*this.t);
            pace_2 =0;
            move = 0;
            man_transform = model_transform.times(Mat4.translation([-11.5,-2, -32.9])).times(Mat4.scale([1,1,1])).times(Mat4.rotation(-Math.PI / 3,Vec.of(1,0,0)));
        
            man2_transform = model_transform.times(Mat4.translation([-8.8,-2, -32.9])).times(Mat4.scale([1,1,1])).times(Mat4.rotation(-Math.PI / 3,Vec.of(1,0,0)));
            
            // console.log(-30 - 1.5*this.t,  -15- 1.2*this.t);
            cam_eye = Vec.of(-37, 4+1*this.t, -61.6+1*this.t);
            cam_at = Vec.of(-16 +1*this.t, -4-1*this.t, -40);

        }
         
          this.draw_human(graphics_state, man_transform, scale_vec1,this.buffalo_skin_body3, this.shirt1, move, 'straight');
          this.draw_human(graphics_state, man2_transform, scale_vec1,this.buffalo_skin_body3, this.shirt2, move, 'straight');
          this.draw_human(graphics_state, teacher_transform, scale_vec1,this.buffalo_skin_body3, this.shirt2, t_move, 'none');
        
        }
graphics_state.camera_transform = Mat4.look_at(cam_eye, cam_at, cam_up);


```


