
// This variable will store the WebGL rendering context
var gl;

window.onload = function init() {
  // Set up a WebGL Rendering Context in an HTML5 Canvas
  var canvas = document.getElementById("gl-canvas");
  // gl = WebGLUtils.setupWebGL(canvas);
  gl = canvas.getContext("webgl");
  if (!gl) {
    alert("WebGL isn't available");
  }

  //  Configure WebGL
  //  eg. - set a clear color
  //      - turn on depth testing
  resize(gl.canvas)

  // gl.clearColor(0.9, 0.9, 0.9, 1.0);

  //  Load shaders and initialize attribute buffers
  var program = initShaders(gl, "vertex-shader", "fragment-shader");
  // var program = webglUtils.createProgramFromScripts(gl,["vertex-shader", "fragment-shader"])
  gl.useProgram(program);


  // Set up data to draw
  // var vertices = [
  //           0.5, 0.5, // point 1
  //            -0.5, -0.5, // point 2
  //            0.5, 0.5,// point 2
  //            -0.5, 0.5
          
  //       ];
  

  
  var colors= 
[
   vec4( 1.0, 0.0, 0.0, 1.0 ), // Triangle 1 is red
   vec4( 1.0, 0.0, 0.0, 1.0 ), 
   vec4( 1.0, 0.0, 0.0, 1.0 ),
   vec4( 0.0, 1.0, 1.0, 1.0 ), // Triangle 2 is cyan
   // vec4( 0.0, 1.0, 1.0, 1.0 ),
   // vec4( 0.0, 1.0, 1.0, 1.0 )
];


// var positions = [
//     100, 200,
//     100, 300,
//     300, 300,
//     300, 200
//   ];

  var positions = [
    50, 200,
    200, 200,
    125, 300,
    // 100, 250,
    125, 175,
    60,275,
    200,275

    // 125, 150

    // 200, 150
  ];

  var indices = 
  [
    0,1,2,
    3,4,5
    // 3
    // 3,1,0

  ];

var size = 3;          // 2 components per iteration
  var type = gl.FLOAT;   // the data is 32bit floats
  var normalize = false; // don't normalize the data
  var stride = 0;        // 0 = move forward size * sizeof(type) each iteration to get the next position
  var offset = 0;        // start at the beginning of the buffer


   var Index_Buffer = gl.createBuffer();
    gl.bindBuffer(gl.ELEMENT_ARRAY_BUFFER, Index_Buffer);
    gl.bufferData(gl.ELEMENT_ARRAY_BUFFER, new Uint16Array(indices), gl.STATIC_DRAW);

  // Load the data into GPU data buffers
// The vertex array is copied into one buffer
  // var vertex_buffer = gl.createBuffer();
  // gl.bindBuffer(gl.ARRAY_BUFFER, vertex_buffer);
  // gl.bufferData(gl.ARRAY_BUFFER, flatten(vertices), gl.STATIC_DRAW);
  // gl.bindBuffer(gl.ARRAY_BUFFER, null);
  // document.write(flatten(vertices))
  // The colour array is copied into another
  var color_buffer = gl.createBuffer();
  gl.bindBuffer(gl.ARRAY_BUFFER, color_buffer);
  gl.bufferData(gl.ARRAY_BUFFER, flatten(colors), gl.STATIC_DRAW);
  
  // for position
    var position_buffer = gl.createBuffer();
  gl.bindBuffer(gl.ARRAY_BUFFER, position_buffer);
  gl.bufferData(gl.ARRAY_BUFFER, flatten(positions), gl.STATIC_DRAW);
  
  
  
  
  //Here we prepare the "vColor" shader attribute entry point to
  //receive RGB float colours from the colour buffer
  
  // var vColor = gl.getAttribLocation(program, "vColor");
  // gl.bindBuffer(gl.ARRAY_BUFFER, color_buffer);
  // gl.vertexAttribPointer(vColor, 0, gl.FLOAT, false, 0, 0);
  // gl.enableVertexAttribArray(vColor);
  


// Associate shader attributes with corresponding data buffers
//Here we prepare the "a_Position" shader attribute entry point to
  //receive 2D float vertex positi:q
  //ons from the vertex buffer
  gl.bindBuffer(gl.ARRAY_BUFFER, position_buffer);
  var a_Position = gl.getAttribLocation(program, "a_Position");
  var u_resolution = gl.getUniformLocation(program, "u_resolution");
  gl.vertexAttribPointer(a_Position, 2, gl.FLOAT, false, 0, 0);
  gl.enableVertexAttribArray(a_Position);


  


// set the resolution
  gl.uniform2f(u_resolution, gl.canvas.width, gl.canvas.height);



  // Get addresses of shader uniforms

  // Either draw as part of initialization
  render(indices.length);

  // Or draw just before the next repaint event
  //requestAnimFrame(render());
};


function render(length) {
   // clear the screen
// Actually, the  WebGL automatically clears the screen before drawing.
  // This command will clear the screen to the clear color instead of white.

  gl.clear(gl.COLOR_BUFFER_BIT);
  gl.clearColor(0.5, 0.5, 0.5, 0.9);
  gl.enable(gl.DEPTH_TEST);
   gl.clear(gl.COLOR_BUFFER_BIT | gl.DEPTH_BUFFER_BIT);
  // gl.clearColor(0, 0, 0, 0);
  // gl.clear(gl.COLOR_BUFFER_BIT);
    gl.viewport(0, 0, gl.canvas.width, gl.canvas.height);
 
  // draw
  // Draw the data from the buffers currently associated with shader variables
  // Our triangle has three vertices that start at the beginning of the buffer.
  // gl.drawArrays(gl.TRIANGLE_FAN, 0, 4);
  // gl.drawArrays(gl.LINE_STRIP, 0, 4);
  gl.drawElements(gl.TRIANGLES, length, gl.UNSIGNED_SHORT,0);


}
