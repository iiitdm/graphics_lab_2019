#include<graphics.h>
#include<math.h>
#include<stdio.h>

#define PI 3.141592654
double to_radian(double arg)
{
  return arg = (arg * PI) / 180; 
}

int main(int argc, char *argv[])
{
  int gd = DETECT, gm;
  initgraph(&gd, &gm, NULL); // very important to call for initiating graphics

  int x1;

  float y1,a;
// /*
  x1 = atoi(argv[1]);
  y1 = atof(argv[2]);
  a = atof(argv[3]);

  int degree;
  double radian, x,y;

  degree = 0.0;
  while(degree <=360)
  {
    radian = to_radian(degree);
    x = (float)((double)a * cos(radian));
    y = (float)((double)a * sin(radian));

    putpixel(round(x + (double)x1), round(y+(double)y1), RED );
    degree+=1;
  }

  getch();
  closegraph(); // has to be called when done
  

  return 0;
}



