#include<graphics.h>
#include<math.h>

/*
void sub_fig(int n,
             float x1, float y1,
             float x2, float y2,
             float x3, float y3);
*/
int depth;

typedef struct point{

  float x,y;
}point;
void fractal_SS(int,point,point,point);

point mid(point p1, point p2);

void draw_triangle(point p1, point p2, point p3);

int main(int argc, char *argv[])
{
  point p1, p2, p3;
  depth = atoi(argv[1]); 
  p1.x = atof(argv[2]);
  p1.y = atof(argv[3]);
  p2.x = atof(argv[4]);
  p2.y = atof(argv[5]);
  p3.x = atof(argv[6]);
  p3.y = atof(argv[7]);

  int gd = DETECT, gm;

  initgraph(&gd, &gm, NULL);


  fractal_SS(1,p1,p2,p3);
  
  getch();

  closegraph();
  
  return 0;
}


// refering to the points clockwise
/*          .t
 *        .   .
 *      .       .
 *    ............
 *    l           r
*/
void fractal_SS(int n,point t, point r, point l)
{
  if(n>=depth)
  {
    draw_triangle(t,r,l);
    return;
  }

  point left_mid, right_mid, bottom_mid;

  left_mid =mid(t,l);
  right_mid = mid(t,r);
  bottom_mid = mid(r,l);
 
  fractal_SS(n+1,t, left_mid, right_mid);
  fractal_SS(n+1, left_mid, bottom_mid, l);
  fractal_SS(n+1, right_mid, r, bottom_mid);
  
    /*
  sub_fig(1,
          (x1+x2)/2, (y1+y2)/2, 
          (x1+x3)/2, (y1+y3)/2,
          (x3+x2)/2, (y3+y2)/2);
  */


}


/*
void sub_fig(int n,
             float x1, float y1,
             float x2, float y2,
             float x3, float y3);
*/

point mid(point p1, point p2)
{
  point mid_pt;
  mid_pt.x = (p1.x+p2.x)/2;
  mid_pt.y = (p1.y+p2.y)/2;
  return mid_pt;
}



// refering to the points clockwise
/*          .t
 *        .   .
 *      .       .
 *    ............
 *    l           r
*/


void draw_triangle(point p1, point p2, point p3)
{
  setcolor(RED);
  line(round(p1.x), round(p1.y), round(p2.x), round(p2.y));
  setcolor(GREEN);
  line(round(p1.x), round(p1.y), round(p3.x), round(p3.y));
  setcolor(BLUE);
  line(round(p3.x), round(p3.y), round(p2.x), round(p2.y));


}
